(define-module (yurei system doas-file)
  #:use-module (gnu system setuid)
  #:use-module (guix gexp)
  #:use-module (gnu)
  #:export (doas-file)
  #:export (%doas-setuid-programs))

(define (doas-file . rules)
  "Create/modify /etc/doas.conf"
  (let ((doas.conf (open-file "/etc/doas.conf" "w")))
    (if (access? "/etc/doas.conf" W_OK)
	(for-each (lambda (rule)
		    (display rule doas.conf)
		    (newline doas.conf))
		  rules)
	(display "Error: cannot write to doas.conf"))
    (close doas.conf)))

(define %doas-setuid-programs
   (map (lambda (file-appendage)
          (setuid-program
           (program (file-append
                     (specification->package (car file-appendage))
                     (cdr file-appendage)))))
	'(("shadow" . "/bin/passwd")
          ("shadow" . "/bin/chfn")
          ("shadow" . "/bin/sg")
          ("shadow" . "/bin/su")
          ("shadow" . "/bin/newgrp")
          ("shadow" . "/bin/newuidmap")
          ("shadow" . "/bin/newgidmap")
          ("inetutils" . "/bin/ping")
          ("inetutils" . "/bin/ping6")
          ("opendoas" . "/bin/doas")
          ;; ("fuse" . "/bin/fusermount")
          ("fuse" . "/bin/fusermount3")
          ("util-linux" . "/bin/mount")
          ("util-linux" . "/bin/umount"))))

