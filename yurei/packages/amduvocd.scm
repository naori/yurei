
(define-module (yurei packages amduvocd)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix licenses))

(define-public amduvocd
  (package
   (name "amduvocd")
   (version "0.1.0")
   (source
    (origin
     (method url-fetch)
     (uri "https://codeberg.org/naori/amd-uvoc/raw/branch/master/amd-uvoc.scm")
     (sha256
      (base32
       "121i9027xzmdbp1s3pbggdwvv84v85l6z5lmrkd0inkma78nfms5"))))
   (arguments
    `(#:install-plan
      '(("amd-uvoc.scm" "bin/amduvocd"))
      #:phases
      ,#~(modify-phases
	  %standard-phases
	  (add-after 'install
		     'make-scripts-executable
		     (lambda _
		       (for-each (lambda (f) (chmod f #o555))
				 (find-files (string-append #$output "/bin"))))))))
   
   (build-system copy-build-system)
   (native-inputs (list (specification->package "guile")))
   (synopsis "Undervolt and fan control daemon for AMD Vega 10")
   (description "Based mainly on https://github.com/grmat/amdgpu-fancontrol")
   (home-page "https://codeberg.org/naori/amd-uvoc")
   (license gpl3+)))

amduvocd
