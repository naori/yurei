(define-module (yurei packages dolphin-emu-current)
  #:use-module (ice-9 match)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix svn-download)
  #:use-module (guix hg-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages emulators)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages assembly)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages cdrom)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cross-base)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages digest)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libedit)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages music)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages upnp)
  #:use-module (gnu packages video)
    #:use-module (gnu packages vulkan)
  #:use-module (gnu packages wxwidgets)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages web)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python))

(define-public dolphin-emu-current
  (let ((commit "dadbeb4bae7e7fa23af2b46e0add4143094dc107")
        (revision "19368"))
    (package
     (name "dolphin-emu-current")
     (version (git-version "5.0" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dolphin-emu/dolphin")
             (commit commit)))
       (file-name (git-file-name name version))
       (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Remove external stuff we don't need.
           (for-each (lambda (dir)
                       (delete-file-recursively
                        (string-append "Externals/" dir)))
                     '("LZO" "OpenAL" "Qt" "SFML" "curl" "ffmpeg"
                       "gettext" "hidapi" "libpng" "libusb" "mbedtls"
                       "miniupnpc" "MoltenVK" "zlib"))
           ;; Clean up source.
           (for-each delete-file (find-files "." ".*\\.(bin|dsy|exe|jar|rar)$"))
           #t))
       (sha256
        (base32
         "1npvaljyc005m7l1l4b8nysk6whbqnhh51mm7zif1arvl44kqi87"))))
     (build-system cmake-build-system)
     (arguments
      '(#:tests? #f
        #:phases
        (modify-phases %standard-phases
		       (add-before 'configure 'generate-fonts&hardcore-libvulkan-path
				   (lambda* (#:key inputs outputs #:allow-other-keys)
					    (let ((fontfile
						   (search-input-file inputs
								      "/share/fonts/truetype/wqy-microhei.ttc"))
						  (libvulkan
						   (search-input-file inputs "/lib/libvulkan.so")))
					      (chdir "docs")
					      (invoke "bash" "-c" "g++ -O2 $(freetype-config \
--cflags --libs) gc-font-tool.cpp -o gc-font-tool")
					      (invoke "./gc-font-tool" "a" fontfile "font_western.bin")
					      (invoke "./gc-font-tool" "s" fontfile "font_japanese.bin")
					      (copy-file "font_japanese.bin" "../Data/Sys/GC/font_japanese.bin")
					      (copy-file "font_western.bin" "../Data/Sys/GC/font_western.bin")
					      (chdir "..")
					      (substitute* "Source/Core/VideoBackends/Vulkan/VulkanLoader.cpp"
							   (("\"vulkan\", 1") (string-append "\"vulkan\""))
							   (("\"vulkan\"") (string-append "\"" libvulkan "\""))
							   (("Common::DynamicLibrary::GetVersionedFilename") ""))
					      #t))))

        ;; The FindGTK2 cmake script only checks hardcoded directories for
        ;; glib/gtk headers.
	#:configure-flags
        (list (string-append "-DX11_INCLUDE_DIR="
                             (assoc-ref %build-inputs "libx11")
                             "/include")
              (string-append "-DX11_LIBRARIES="
                             (assoc-ref %build-inputs "libx11")
                             "/lib/libX11.so")
              "-DX11_FOUND=1")))
     (native-inputs
      `(("pkg-config" ,pkg-config)
        ("gettext" ,gettext-minimal)))
     (inputs
      (list alsa-lib
            ao
            bluez
            curl
            eudev
            ffmpeg-4
            font-wqy-microhei
            freetype
            glew
            glib
            glu
            gtk+-2
            hidapi
            libevdev
            libpng
            libusb
            libx11
            libxi
            libxrandr
            lzo
            mbedtls-apache
            mesa
            miniupnpc
            openal
            pugixml
            pulseaudio
            qtbase-5
            sdl2
            sfml
            soil
            soundtouch
            vulkan-loader
            zlib))
     (home-page "https://dolphin-emu.org/")
     (synopsis "Nintendo Wii and GameCube emulator")
     (description
      "Dolphin is an emulator for two Nintendo video game consoles: the
GameCube and the Wii.  It provides compatibility with all PC controllers,
turbo speed, networked multiplayer, and graphical enhancements.")
     (supported-systems '("x86_64-linux" "aarch64-linux"))
					; dolphin/Data/Sys/GC/font_*.bin: Licensed under ASL2.0.
     (license (list license:gpl2+ license:asl2.0 license:fdl1.2+)))))

dolphin-emu-current
