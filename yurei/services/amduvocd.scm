(define-module (yurei services amduvocd)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (yurei packages amduvocd)
  #:use-module (ice-9 textual-ports)
  #:export (amduvocd-configuration
	    amduvocd-service-type
	    amduvocd-service))

(use-modules (ice-9 textual-ports))

(define default-fan-pwms
  (list 20000 24
	37000 30
	52000 45
	67000 45
	78000 110
	85000 150))
(define default-sclk-voltages
  (list 0 560 800
	1 775 830
	2 852 860
	3 991 900
	4 1138 930
	5 1269 960
	6 1350 970
	7 1500 990))
(define default-mclk-voltages
  (list 0 167 800
	1 500 800
	2 800 910
	3 945 965))

(define-record-type* <amduvocd-configuration>
  amduvocd-configuration make-amduvocd-configuration
  amduvocd-configuration?
  (amduvocd                amduvocd-configuration-amduvocd ; <package>
			   (default amduvocd))
  (amduvocd-fan-pwms       amduvocd-configuration-fan-pwms ; list of integers
			   (default default-fan-pwms))
  (amduvocd-sclk-voltages  amduvocd-configuration-sclk-voltages ; list of integers
			   (default default-sclk-voltages))
  (amduvocd-mclk-voltages  amduvocd-configuration-mclk-voltages ; list of integers
			   (default default-mclk-voltages)))

(define (amduvocd-shepherd-service config)  
  (let (;(fan-pwms (map number->string (amduvocd-configuration-fan-pwms config)))
	;(sclk-voltages (map number->string (amduvocd-configuration-sclk-voltages config)))
	;(mclk-voltages (map number->string (amduvocd-configuration-mclk-voltages config)))
	(amduvocd (amduvocd-configuration-amduvocd config)))
    (list (shepherd-service
	   (documentation "Set SCLK and MCLK voltages, and start fan control daemon")
	   (provision '(amduvocd))
	   (requirement '(user-processes))
	   (start
	    #~(make-forkexec-constructor
	       (append ; creates list "package/bin/amduvocd" "20000" "100" ... "-s" "852" "100" ... "-m" "900" "900" ...
		(list (string-append #$amduvocd "/bin/amduvocd"))
		(list #$@(map number->string (amduvocd-configuration-fan-pwms config)))
		(list "-s") (list #$@(map number->string (amduvocd-configuration-sclk-voltages config)))
		(list "-m") (list #$@(map number->string (amduvocd-configuration-mclk-voltages config))))
	       #:create-session? #t
	       ;; #:log-file "/var/log/amduvocd.log"
	       ))
	   (stop #~(make-kill-destructor))))))

(define amduvocd-service-type
  (service-type
   (name 'amduvocd)
   (description "AMD UVOC daemon")
   (extensions
    (list
     (service-extension shepherd-root-service-type
			amduvocd-shepherd-service)))))

(define* (amduvocd-service #:key (amduvocd amduvocd)
			   (amduvocd-fan-pwms default-fan-pwms)
			   (amduvocd-sclk-voltages default-sclk-voltages)
			   (amduvocd-mclk-voltages default-mclk-voltages))
  "Return a service that runs @var{amduvocd}"
  (service amduvocd-service-type
	   (amduvocd-configuration
	    (amduvocd amduvocd)
	    (amduvocd-fan-pwms amduvocd-fan-pwms)
	    (amduvocd-sclk-voltages amduvocd-sclk-voltages)
	    (amduvocd-mclk-voltages amduvocd-mclk-voltages))))
